import 'package:flutter/material.dart';
import 'package:weather/weather.dart';
import 'package:dio/dio.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';

Dio dio = Dio();

Future<Position> determinePosition() async {
  bool serviceEnable;
  LocationPermission permission;

  serviceEnable = await Geolocator.isLocationServiceEnabled();
  if (serviceEnable == false) {
    await Geolocator.openLocationSettings();
    return Future.error('Location services are disable');
  }

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are dined');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error(
        'Location permissions are permanently denied, we cannot request permissions.');
  }

  return await Geolocator.getCurrentPosition();
}

Future<List<Placemark>> getAddressFromLatLong(Position position) async {
  List<Placemark> placeMark =
      await placemarkFromCoordinates(position.latitude, position.longitude);
  return placeMark;
}

Future<List<Weather>?> getWeatherData() async {
  Response response = await dio.get(
      'https://api.openweathermap.org/data/2.5/forecast?appid=358ec1d9e2845478e10a32598938d416&units=metric&id=1581130&lang=vi');

  if (response.statusCode == 200) {
    final listWeather = response.data['list'];

    if (listWeather is List) {
      return listWeather.map((e) => Weather.fromJson(e)).toList();
    }
    return null;
  } else {
    throw Exception('Get List Weather Failed');
  }
}

class Content extends StatefulWidget {
  const Content({super.key});

  @override
  State<Content> createState() => _ContentState();
}

class _ContentState extends State<Content> {
  int day = 0;
  List<Weather> weathers = [];
  DateTime date = DateTime.now();
  List<Placemark> address = List.empty();

  static const IconData location_on =
      IconData(0xe3ab, fontFamily: 'MaterialIcons');

  void getLocation() async {
    Position position = await determinePosition();
    List<Placemark> place = await getAddressFromLatLong(position);
    setState(() {
      address = place;
    });
  }

  void getListWeathers() async {
    final result = await getWeatherData();
    if (result is List) {
      setState(() {
        weathers = result ?? [];
      });
    }
  }

  Widget weatherInfo(String url, String info) {
    return SizedBox(
      height: 100,
      width: 100,
      child: Column(
        children: [
          Expanded(
              child: Image.network(
            url,
            width: 35,
          )),
          Expanded(
              child: Text(
            info,
            style: const TextStyle(fontSize: 13),
            textAlign: TextAlign.center,
          )),
        ],
      ),
    );
  }

  String getDay(DateTime date) {
    if (date.weekday == 1) return 'Mon';
    if (date.weekday == 2) return 'Tue';
    if (date.weekday == 3) return 'Wed';
    if (date.weekday == 4) return 'Thu';
    if (date.weekday == 5) return 'Fri';
    if (date.weekday == 6) return 'Sat';
    if (date.weekday == 7) return 'Sun';
    return '';
  }

  @override
  void initState() {
    super.initState();
    getListWeathers();
    getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                /*-------------------------------Today Temperature----------------------------------- */
                if (weathers.isNotEmpty)
                  Stack(
                    children: [
                      Image.asset('assets/images/weather3.jpg'),
                      Container(
                        padding: const EdgeInsets.all(5),
                        child: Row(
                          children: [
                            const Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
                              child: Icon(
                                location_on,
                                color: Colors.red,
                                size: 20,
                              ),
                            ),
                            if (address.isNotEmpty)
                              Text(
                                '${address[0].subAdministrativeArea}, ${address[0].administrativeArea}',
                                style: const TextStyle(color: Colors.red),
                              ),
                          ],
                        ),
                      ),
                      Container(
                          padding: const EdgeInsets.fromLTRB(0, 55, 0, 0),
                          child: Center(
                            child: Text(
                              '${weathers[0].main.temp.round().toString()}\u2103',
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 75,
                              ),
                            ),
                          )),
                      Container(
                        padding: const EdgeInsets.fromLTRB(0, 155, 0, 0),
                        child: Center(
                          child: Text(
                            weathers[0].weather[0].main,
                            style: const TextStyle(
                                color: Colors.white, fontSize: 25),
                          ),
                        ),
                      ),
                    ], /*-------------------------------Max - Min Temperature----------------------------- */
                  ),
                if (weathers.isNotEmpty)
                  Container(
                    padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Today  ${getDay(date)}',
                          style: const TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 15),
                        ),
                        Text(
                          '${weathers[0].main.temp_min.round()}\u2103 ~ ${weathers[0].main.temp_max.round()}\u2103',
                          style: const TextStyle(
                              fontWeight: FontWeight.w900, fontSize: 15),
                        ),
                      ],
                    ),
                  ),
                /*-------------------------------List Weather Horizontal----------------------------- */
                if (weathers.isNotEmpty)
                  Container(
                    padding: const EdgeInsets.all(5),
                    height: 100,
                    child: ListView.builder(
                      physics: const ClampingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: 7,
                      itemBuilder: ((context, index) {
                        return Container(
                          width: 100,
                          padding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
                          margin: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                          child: Column(
                            children: [
                              Text(weathers[index].dt_txt?.substring(11, 16) ??
                                  ''),
                              Expanded(
                                child: Image.network(
                                    'https://openweathermap.org/img/wn/${weathers[index].weather[0].icon}@2x.png'),
                              ),
                              Text(
                                  '${weathers[index].main.temp.round().toString()}\u2103')
                            ],
                          ),
                        );
                      }),
                    ),
                  ),
                /*-------------------------------List Weather Vertical----------------------------- */
                if (weathers.isNotEmpty)
                  Container(
                      padding: const EdgeInsets.fromLTRB(15, 5, 10, 15),
                      height: 255,
                      child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: 5,
                        itemBuilder: (context, index) {
                          if (day != 0) {
                            day += 8;
                          } else {
                            day += 5;
                          }
                          if (day >= 40) {
                            day %= 40;
                          }
                          return SizedBox(
                            height: 50,
                            child: Row(
                              children: [
                                Text(
                                    '${weathers[day].dt_txt?.substring(8, 10)}/${weathers[day].dt_txt?.substring(5, 7)}'),
                                Expanded(
                                  child: Image.network(
                                      'https://openweathermap.org/img/wn/${weathers[day].weather[0].icon}@2x.png'),
                                ),
                                Text(
                                    '${weathers[day].main.temp.round().toString()}\u2103')
                              ],
                            ),
                          );
                        },
                      )),
                /*-------------------------------Weather Info----------------------------- */
                if (weathers.isNotEmpty)
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        weatherInfo(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4HYQb1_XPb_4AQIJDImtSTh-JstKNmOyrOg&usqp=CAU',
                            'Temperature Felt ${weathers[0].main.temp_max.round().toString()}\u2103'),
                        weatherInfo(
                            'https://cdn-icons-png.flaticon.com/512/728/728093.png',
                            'Humidity ${weathers[0].main.humidity.toString()} %'),
                        weatherInfo(
                            'https://cdn-icons-png.flaticon.com/512/4005/4005908.png',
                            'Visibility ${weathers[0].visibility.toString()} m'),
                      ],
                    ),
                  ),
                if (weathers.isNotEmpty)
                  Container(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        weatherInfo(
                            'https://media.istockphoto.com/id/1368763816/vector/wind-blow-vector-icon-air-puff-symbol.jpg?s=612x612&w=0&k=20&c=0GIijczbW0qFbHtaPWS9tf-QUSYCRr627AkUp5I0jRo=',
                            'ENE ${weathers[0].wind.speed.round().toString()} km/h'),
                        weatherInfo(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTh71CQCLTCCIE9rBjf8bxx2tjyrlr1GxyFP2mshsU&s',
                            'UV Weak'),
                        weatherInfo(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_RzaA1RfKCQNmdBFy4azvI7oNwiflyegARQ&usqp=CAU',
                            'Air Pressure ${weathers[0].main.pressure.toString()} hPa'),
                      ],
                    ),
                  ),
              ]),
        ],
      ),
    );
  }
}
