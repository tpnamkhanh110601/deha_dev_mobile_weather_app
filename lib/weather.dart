import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'weather.freezed.dart';
part 'weather.g.dart';

@freezed
class Main with _$Main {
  const factory Main({
    required double temp,
    required double feels_like,
    required double temp_min,
    required double temp_max,
    required int pressure,
    required int sea_level,
    required int grnd_level,
    required int humidity,
    required double temp_kf,
  }) = _Main;

  factory Main.fromJson(Map<String, Object?> json) => _$MainFromJson(json);
}

@freezed
class Wind with _$Wind {
  const factory Wind({
    required double speed,
    required int deg,
    required double gust,
  }) = _Wind;

  factory Wind.fromJson(Map<String, Object?> json) => _$WindFromJson(json);
}

@freezed
class Clouds with _$Clouds {
  const factory Clouds({
    required int all,
  }) = _Clouds;

  factory Clouds.fromJson(Map<String, Object?> json) => _$CloudsFromJson(json);
}

@freezed
class Sys with _$Sys {
  const factory Sys({
    required String pod,
  }) = _Sys;

  factory Sys.fromJson(Map<String, Object?> json) => _$SysFromJson(json);
}

@freezed
class WeatherElement with _$WeatherElement {
  const factory WeatherElement({
    required int id,
    required String main,
    required String description,
    required String icon,
  }) = _WeatherElement;

  factory WeatherElement.fromJson(Map<String, Object?> json) =>
      _$WeatherElementFromJson(json);
}

@freezed
class Rain with _$Rain {
  const factory Rain({
    String? threeHours,
  }) = _Rain;

  factory Rain.fromJson(Map<String, Object?> json) => _$RainFromJson(json);
}

@freezed
class Weather with _$Weather {
  const factory Weather(
      {required int dt,
      required Main main,
      required List<WeatherElement> weather,
      required Clouds clouds,
      required Wind wind,
      required int visibility,
      required double pop,
      Rain? rain,
      required Sys sys,
      String? dt_txt,
      required}) = _Weather;

  factory Weather.fromJson(Map<String, Object?> json) =>
      _$WeatherFromJson(json);
}
